import Bootcamp from "./lib/bootcamp";

const args = process.argv.slice(2)

const command = args[0]

switch(command){
    case "register":
        //console.log(args[1])
        Bootcamp.register(args[1])
        break;

    case "login":
        Bootcamp.login(args[1])
        break;
    case "addSiswa":
        Bootcamp.addSiswa(args[1])
        break;

    default:
        break;
}