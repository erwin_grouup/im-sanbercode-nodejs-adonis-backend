class Employee{
    constructor(name,password,role){
        this._name=name;
        this._password=password;
        this._role=role;
        this._isLogin=false;
    }


    get name(){
        return this._name;
    }

    set name(new_name){
        this._name=new_name;
    }

    get password(){
        return this._password;
    }

    set password(new_password){
        this._password=new_password;
    }

    get role(){
        return this._role;
    }

    set role(new_role){
        this._role=new_role;
    }


    get isLogin(){
        return this._isLogin;
    }

    set isLogin(status){
        this._isLogin=status;
    }
    
}

export default Employee;