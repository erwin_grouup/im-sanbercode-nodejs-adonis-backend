import Employee from "./employee";
import fs from "fs";
import "core-js/stable"
import { stringify } from "querystring";
import fsPromise from "fs/promises"

const path ="data.json"

class Bootcamp{
    static register(input){
        let [name,password,role] =input.split(",");
        fs.readFile(path,(err,data)=>{
            if(err){
                console.log(err);
            }

            let existingData = JSON.parse(data);
            let employee = new Employee(name,password,role);
            existingData.push(employee);
            fs.writeFile(path,JSON.stringify(existingData),(err)=>{
                
                if(err){
                    console.log(err);
                }
                else{
                    console.log("berhasil");
                }
            });
        });
    }

    static login(input){
        let [name,password] =input.split(",")

        fs.promises.readFile(path)
        .then((data) => {
            let employees =  JSON.parse(data);

            let indexEmp  = employees.findIndex((emp)=>emp._name==name);

            if(indexEmp==-1){
                console.log("Data tidak ditemukan");
            }else{
                let employee=employees[indexEmp]
                
                if(employee._password==password){
                    employee._isLogin=true;

                    
                   employees.splice(indexEmp,1,employee);
                   return fsPromise.writeFile(path, JSON.stringify(employees));
                }else{
                    console.log("Password yg dimasukkan salah");
                }
            }

        })
        .catch((err) => {
            console.log(err)
        })
        

    }


    static addSiswa = async (param) =>{
        let arr=param.split(",")
        let student =arr[0]
        let trainer =arr[1]
        let dataRead = await fsPromise.readFile(path)
        let employees = JSON.parse(dataRead)

        let indexEmp  = employees.findIndex((emp)=>emp._name==trainer);

        if(indexEmp==-1){
            console.log("Data trainer tidak ditemukan");
        }else{
            let employee=employees[indexEmp]
            
            if(employee._isLogin==true){
                    let studentaTemp = {
                        name: student
                    }

                    let arr_student=new Array(studentaTemp);

                    let newData={
                        _name:employee._name,
                        _password:employee._password,
                        _role:employee._role,
                        _isLogin:employee._isLogin,
                        _student:arr_student

                    }
                    console.log(newData);
                    employees.splice(indexEmp,1,newData);
                    await fsPromise.writeFile(path, JSON.stringify(employees));
                    console.log("Data siswa berhasil ditambahkan");

            }else{
                console.log("Trainer Belum login");
            }
        }

    }
}

export default Bootcamp;