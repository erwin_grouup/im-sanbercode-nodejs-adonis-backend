function hitungVokal(str){
    var vokal = new Array("a","i","u","e","o");
    var counter=0;
    for (var i = 0; i < str.length; i++) {
        for (let index = 0; index < vokal.length; index++) {
            var huruf=str[i].toLowerCase()
            if(huruf==vokal[index]){
                counter++
            }
        }
    }
    return counter;

}
    
    // Test Cases
    
    console.log(hitungVokal("Adonis"))// Output:3
    
    console.log(hitungVokal("Error"))//Output:2
    
    console.log(hitungVokal("Eureka"))//Output:4
    
    console.log(hitungVokal("Rsch")) // Output: 0