function graduate(arr) {

    var res_array=new Array();
    var result=new Object();
    for (let index = 0; index < arr.length; index++) {
        var predikat="";
       if(arr[index].score<60){
            predikat="participated"
       }else if(arr[index].score<=85 && arr[index].score>=60){
            predikat="completed"
       }else{
            predikat="mastered"
       }
       var temp_data={
                name:arr[index].name,score:arr[index].score, grade: predikat
       }
       if(!result[arr[index].class]){
        result[arr[index].class]=new Array(temp_data);
       }else{
        result[arr[index].class].push(temp_data);
       }
       
    }
    return result
}
    
    // TEST CASE
    // Contoh Input
    
    var arr = [
    
    {name:"Ahmad",score:80, class: "Laravel"},
    
    {name:"Regi",score:86, class: "Vuejs"},
    
    {name:"Robert",score:59, class: "Laravel"},
    
    {name:"Bondra",score:81, class: "Reactjs" }
    
    ]
    
    
    console.log(graduate(arr))
    
    //Output
    
    // {
    
    // Laravel:[{name:"Ahmad",score:80, grade: "completed"}, { name: "Robert", score: 59, grade: "participated"}],
    
    // Vuejs:[
    
    // {name:"Regi",score:86, grade: "mastered"}
    
    // ],
    
    // Reactjs:[{name:"Bondra",score:81, grade: "completed"}]
    
    // }