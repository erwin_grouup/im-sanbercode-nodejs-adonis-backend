function kelompokAngka(arr){

    var result_kelipatan3=new Array();
    var result_ganjil=new Array();
    var result_genap=new Array();

    for (let index = 0; index < arr.length; index++) {
       if(arr[index] % 3==0){
            result_kelipatan3.push(arr[index])
       }else if(arr[index] % 2==0){
            result_genap.push(arr[index])
       }else{
            result_ganjil.push(arr[index])
       }
    }

    return Array(result_ganjil,result_genap,result_kelipatan3)

}

//Test Case

console.log(kelompokAngka([1,2,3,4,5,6,7]))

//Output:[[1,5,7],[2,4],[3,6]]

console.log(kelompokAngka([13,27,11,15]))

//Output:[[13,11],[],[27,15]]

console.log(kelompokAngka([]))

//output:[[],[],[]]