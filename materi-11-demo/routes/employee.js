var express = require('express');
var router = express.Router();

const EmployeeController= require('../controllers/employeeController');

router.post('/register', EmployeeController.register);
router.get('/register', EmployeeController.showAllEmploye);

router.post('/login', EmployeeController.login);

router.post('/karyawan/:name/siswa', EmployeeController.addSiswa);

module.exports = router;
