const fs= require('fs')
const fsPromises = require('fs/promises')

const path='data.json'

class employeeController{

    static register(req,res){
        //res.status(200).json({message:"testing"})

        fs.readFile(path,(err, data) => {
            if(err){
                res.status(400).json({
                    error: "error data tidak terbaca"
                })
            }else{
                let existtingData = JSON.parse(data)

                let {users} = existtingData

                let {name, role, password} = req.body
                let newEmployee = {name,role,password, isLogin:false}


                users.push(newEmployee)

                let newData = {...existtingData,users}

                fs.writeFile(path, JSON.stringify(newData),(err)=>{
                    if(err){
                        res.status(400).json({
                            error: "error menyimpan data"
                        })
                    }else{
                        res.status(201).json({
                            message : "Berhasil Register",
                            new_data : newEmployee
                        })
                    }
                })

            }
        });
    }

    static showAllEmploye(req,res){
        fs.readFile(path,(err, data) => {
            if(err){
                res.status(400).json({
                    error: "error data tidak terbaca"
                })
            }else{
                let existtingData = JSON.parse(data)

                
                if(err){
                    res.status(400).json({
                        error: "error menyimpan data"
                    })
                }else{
                    res.status(201).json({
                        message : "Berhasil get karyawan",
                        data : existtingData.users
                        
                    })
                }
                

            }
        });
    }

    static login(req,res){
        
        fsPromises.readFile(path)
        .then((data) => {
            let employees =  JSON.parse(data);

            let {users} = employees
            
            let {name, password} = req.body

            let indexEmp  = users.findIndex((user)=>user.name == name);

            if(indexEmp==-1){
                res.status(400).json({
                    error: "Data "+name+" tidak ditemukan ",
                })
            }else{
                let employee=users[indexEmp]
                
                if(employee.password==password){
                    employee.isLogin=true;

                    
                    users.splice(indexEmp,1,employee);
                   return fsPromises.writeFile(path, JSON.stringify(employees));
                }else{
                    res.status(400).json({
                        error: "Password yang dimasukkan salah"
                    })
                }
            }

        })
        .then(()=>{
            res.status(200).json({
                message: "Login Berhasil"
            })
        })
        .catch((err) => {
            console.log(err)
        })


    }

     static async addSiswa(req,res){
       
        let student = req.body.name
        let kelas =req.body.class
        let trainer =req.params.name
        let dataRead = await fsPromises.readFile(path)
        let employees = JSON.parse(dataRead)

        let {users} = employees

        let indexEmp  = users.findIndex((emp)=>emp.name==trainer);

        if(indexEmp==-1){
            console.log("Data trainer tidak ditemukan");
        }else{
            let employee=users[indexEmp]
            
            if(employee.isLogin==true){
                if(employee.role=='admin'){
                    let studentaTemp = {
                        name: student,
                        class:kelas
                    }

                    if(employee.student){
                        employee.student.push(studentaTemp)
                    }else{
                        employee.student=new Array(studentaTemp);
                    }
                    
                    
                    users.splice(indexEmp,1,employee);
                    await fsPromises.writeFile(path, JSON.stringify(employees));
                    res.status(200).json({
                        message: "Berhasil add siswa"
                    })
                }else{
                    res.status(400).json({
                        message: "Trainer Bukan Admin"
                    })
                }
                    

            }else{
                res.status(400).json({
                    message: "Trainer Belum Login"
                })
            }
        }
  
    }
}

module.exports = employeeController