function arrayToObject(arr) {

    if(arr.length>0){ 
        console.log("=========================");   
        for (let index = 0; index < arr.length; index++) {
           
            var umur="Invalid Brithday";
            if(arr[index][3]){
                var now = new Date()
                var thisYear = now.getFullYear() // 2020 (tahun sekarang)
                if(arr[index][3]<thisYear){
                    umur=thisYear-arr[index][3];
                }
                
            }
            var obj = {
                firstName: arr[index][0],
                lastname: arr[index][1],
                gender: arr[index][2],
                age: umur
            } 
            
            console.log((index+1)+"  "+obj.firstName+" "+obj.lastname+":");    
            console.log(obj);       
        }
        console.log("=========================");   
    }else{
        console.log("Array tidak boleh kosong");
    }

}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


console.log("================ SOAL NOMOR 2 =====================");


function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if(arrPenumpang.length>0){
         //your code here
        var result= new Array();
        for (let a = 0; a < arrPenumpang.length; a++) {
            var rute1=arrPenumpang[a][1];
            var rute2=arrPenumpang[a][2];
            var temp_index1;
            var temp_index2;
            for (var index = 0; index < rute.length; index++) {
                if(rute[index]==rute1){
                    temp_index1=index;
                }
                if(rute[index]==rute2){
                    temp_index2=index;
                }
            }
            var bayar=0;
            bayar=(temp_index2-temp_index1)*2000
            var obj = {
                penumpang: arrPenumpang[a][0],
                naikDari: arrPenumpang[a][1],
                tujuan: arrPenumpang[a][2],
                bayar: bayar
            } 
            if(obj){
                result.push(obj)
            }
        }
        return result
    }else{
        return "array penumpang harus disi";
    }
    
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]



  console.log("================ SOAL NOMOR 3 =====================");

  function nilaiTertinggi(siswa) {
    var nilai_tertinggi=new Array();
    var result=new Object();
    for (let index = 0; index < siswa.length; index++) {
        if(!nilai_tertinggi){
            nilai_tertinggi.push(siswa[index]);
        }else{
            var data_baru=1;
            for (let a = 0; a < nilai_tertinggi.length; a++) {
                if(nilai_tertinggi[a].class==siswa[index].class){
                    if(nilai_tertinggi[a].score<siswa[index].score){
                        nilai_tertinggi[a]=siswa[index]
                        data_baru=0;
                        break;
                    }else{
                        data_baru=0
                        break;
                    }
                }
            }
            if(data_baru==1){
                nilai_tertinggi.push(siswa[index]);
            }
        }
    }
   
    for (let b = 0; b < nilai_tertinggi.length; b++) {
        result[nilai_tertinggi[b].class] ={
            name:nilai_tertinggi[b].name,
            score:nilai_tertinggi[b].score,
        }
        
    }
    return result
  }
  
  // TEST CASE
  console.log(nilaiTertinggi([
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]));
  
  // OUTPUT:
  
  // {
  //   adonis: { name: 'Asep', score: 90 },
  //   vuejs: { name: 'Ahmad', score: 85 },
  //   reactjs: { name: 'Afrida', score: 78}
  // }
  
  
  console.log(nilaiTertinggi([
    {
      name: 'Bondra',
      score: 100,
      class: 'adonis'
    },
    {
      name: 'Putri',
      score: 76,
      class: 'laravel'
    },
    {
      name: 'Iqbal',
      score: 92,
      class: 'adonis'
    },
    {
      name: 'Tyar',
      score: 71,
      class: 'laravel'
    },
    {
      name: 'Hilmy',
      score: 80,
      class: 'vuejs'
    }
  ]));
  
  // {
  //   adonis: { name: 'Bondra', score: 100 },
  //   laravel: { name: 'Putri', score: 76 },
  //   vuejs: { name: 'Hilmy', score: 80 }
  // }