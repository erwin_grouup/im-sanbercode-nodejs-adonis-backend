import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import VenueValidator from 'App/Validators/VenueValidator';
import {schema, rules } from '@ioc:Adonis/Core/Validator';
import Database from '@ioc:Adonis/Lucid/Database';
export default class VenuesController {
  public async index({response}: HttpContextContract) {

    try {

      const venues = await Database
      .from('venues') // 👈 gives an instance of select query builder
      .select('*')

      response.ok({
        response:'success',
        data: venues
      })
    } catch (error) {
      response.badRequest(error.messages)
    }



  }

  public async create({}: HttpContextContract) {}

  public async store({response,request}: HttpContextContract) {

    try {
      const newVenues= await request.validate(VenueValidator)

      await Database
          .table('venues')
          .insert({
            name:newVenues.name,
            address:newVenues.address,
            phone:newVenues.phone,
          })

      response.ok({
        response:'success',
        message: newVenues
      })
    } catch (error) {
        response.badRequest(error.messages)
    }
  }

  public async show({response,params}: HttpContextContract) {

    try {

      const venues = await Database
        .from('venues')
        .where('id',params.id)
        .first()

      response.ok({
        response:'success',
        data: venues
      })
    } catch (error) {
      response.badRequest(error.messages)
    }

  }

  public async edit({}: HttpContextContract) {




  }

  public async update({response,request,params}: HttpContextContract) {

    try {

      const newVenues= await request.validate(VenueValidator)

      const venues=await Database
      .from('venues')
      .where('id',params.id)
      .update({
        name:newVenues.name,
        address:newVenues.address,
        phone:newVenues.phone,
      })

      response.ok({
        response:'success',
        message: "data berhasil diubah"
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async destroy({response,params}: HttpContextContract) {

    try {

      const venues = await Database
        .from('venues')
        .where('id',params.id)
        .delete()

      response.ok({
        response:'success',
        message: 'data berhasil dihapus'
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }
}
