import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import FieldValidator from 'App/Validators/FieldValidator'
import ParamsFieldValidator from 'App/Validators/ParamsFieldValidator'

export default class FieldsController {
  public async index({response,params}: HttpContextContract) {

    try {

      const venues = await Database
      .from('fields') // 👈 gives an instance of select query builder
      .select('*')
      .where('venue_id',params.venue_id)

      response.ok({
        response:'success',
        data: venues
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async create({}: HttpContextContract) {}

  public async store({response,request,params}: HttpContextContract) {

    try {

      const newFieldsVal= await request.validate(FieldValidator)
      const paramsFieldVal= await params.validate(ParamsFieldValidator)

      const newFields=await Database
          .table('fields')
          .insert({
            name:newFieldsVal.name,
            type:newFieldsVal.type,
            venue_id:paramsFieldVal.venue_id,
          })

      response.ok({
        response:'success',
        message: newFields
      })
    } catch (error) {
        response.badRequest(error.messages)
    }
  }

  public async show({response,params}: HttpContextContract) {

    try {

          const venues = await Database
            .from('fields')
            .where('venue_id',params.venue_id)
            .where('id',params.id)
            .first()

          response.ok({
            response:'success',
            data: venues
          })
        } catch (error) {
          response.badRequest(error.messages)
        }
  }

  public async edit({}: HttpContextContract) {}

  public async update({response,request,params}: HttpContextContract) {


    try {

      const newFieldsVal= await request.validate(FieldValidator)
      const paramsFieldVal= await params.validate(ParamsFieldValidator)

      const venues=await Database
      .from('fields')
      .where('id',params.id)
      .update({
        name:newFieldsVal.name,
        type:newFieldsVal.type,
        venue_id:paramsFieldVal.venue_id,
      })

      response.ok({
        response:'success',
        message: "data berhasil diubah"
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async destroy({response,params}: HttpContextContract) {

    try {

      const venues = await Database
        .from('fields')
        .where('id',params.id)
        .delete()

      response.ok({
        response:'success',
        message: 'data berhasil dihapus'
      })
    } catch (error) {
      response.badRequest(error.messages)
    }

  }

}
