var express = require('express');
var router = express.Router();

/* GET home page. */
const VenuesController= require('../controllers/venuesController');

router.post('/venues', VenuesController.store);

router.get('/venues', VenuesController.index);

router.get('/venues/:id', VenuesController.getDataByID);

router.put('/venues/:id', VenuesController.updateDataByID);

router.delete('/venues/:id', VenuesController.getDeleteDataByID);


module.exports = router;
