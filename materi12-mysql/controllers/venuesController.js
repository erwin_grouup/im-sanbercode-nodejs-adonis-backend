const fs= require('fs')
const fsPromises = require('fs/promises');
const { title } = require('process');
const path='data.json'

const {venues} =require("../models");

class VenuesController{

    static async index(req,res){

        try {
            const venue = await venues.findAll();
            console.log("All users:", JSON.stringify(venue, null, 2));
            res.status(200).json({
                status : "success",
                message : "Berhasil mendapatkan data",
                data : venue
            })
        }catch(error){
            res.status(401).json({
                status : "Failed",
                message : "Gagal mendapatkan data",
                msg:error.errors
            })
        }
    }

    static async store(req,res){
        try {
            let name = req.body.name
            let address = req.body.address
            let phone = req.body.phone


            const newVenues = await  venues.create({
                name:name,
                address:address,
                phone:phone
            })

            res.status(200).json({
                status : "success",
                message : "Postingan berhasil ditambahkan"
            })
        } catch (error) {
            res.status(401).json({
                status : "Failed",
                message : "Gagal menyimpan data",
                msg:error.errors
            })
        }
         

    }

    static async getDataByID(req,res){

        try {

            let id =req.params.id
            
            const venue = await venues.findAll({
                where: {
                    id: id
                }
                });

            if(venue === undefined || venue.length == 0){
                
                res.status(401).json({
                    status : "success",
                    message : "Data Kosong",
                    msg:"id tidak ditemukan"
                })

            }else{
                
                res.status(200).json({
                    status : "success",
                    message : "Berhasil mendapatkan data",
                    data : venue
                })
            }
        } catch (error) {
            res.status(401).json({
                status : "Failed",
                message : "Gagal menampilkan data",
                msg:error.errors
            })
        }
    }


    static async updateDataByID(req,res){
        try {
            let name = req.body.name
            let address = req.body.address
            let phone = req.body.phone
            let id =req.params.id
            let string_temp=""

            const updatedVenues = await venues.update({
                name:name,
                address:address,
                phone:phone
            }, 
            {
                where: {
                    id: id
                }
            });

            res.status(200).json({
                status : "success",
                message : "Postingan berhasil diupdate"
            })
        } catch (error) {
            res.status(401).json({
                status : "Failed",
                message : "Gagal mengupdate data",
                msg:error.errors
            })
        }
    }



    static async getDeleteDataByID(req,res){

        try {

            let id =req.params.id
            
            const venue = await venues.destroy({
                where: {
                  id: id
                }
              });

           
            if(venue){
                res.status(200).json({
                    status : "success",
                    message : venue,
                    msg:"data berhasil dihapus"
                })
            }else{
                res.status(200).json({
                    status : "success",
                    message : "data tidak ada",
                    msg:"data tidak ada yang dihapus"
                })
            }
           
                
           
        } catch (error) {
            res.status(401).json({
                status : "Failed",
                message : "Gagal menghapus data",
                msg:error.errors
            })
        }
    }

}

module.exports = VenuesController;