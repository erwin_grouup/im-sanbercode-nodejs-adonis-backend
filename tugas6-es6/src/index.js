



import {sapa} from './lib/sapa.js'
import {convert} from './lib/convert.js'
import {checkScore} from './lib/checkScore.js'
import {filter} from './lib/filter.js'


const myArgs =  process.argv.slice(2)
const command=  myArgs[0]

switch (command){
    case 'sapa':
        let nama = myArgs[1]
        console.log(sapa(nama))
        break;
    case 'convert':
        const params = myArgs.slice(1)
        let [nama2,domisili,umur] =params

        console.log(convert(nama2,domisili,umur))
        break;
    case 'checkScore':
        const paramscheckScore = myArgs.slice(1)
        
        let arr=paramscheckScore[0].split(",")
        
        for (let i = 0; i < arr.length; i++) {
            let tempVar=arr[i].split(":")
            if(tempVar[0]=="name"){
                var nama3=tempVar[1]
            }else if(tempVar[0]=="kelas"){
                var kelas=tempVar[1]
            }else if(tempVar[0]=="score"){
                var score=tempVar[1]
            }  
        }

        console.log(checkScore(nama3,kelas,score))
        break;
    case 'filter':
        let a = myArgs[1]
        console.log(filter(a))
        break;
    default:
        console.log('Sorry, that is not something I know how to do.')

}