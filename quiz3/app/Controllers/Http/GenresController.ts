import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {schema, rules } from '@ioc:Adonis/Core/Validator';
import Database from '@ioc:Adonis/Lucid/Database';

export default class GenresController {
  public async index({response}: HttpContextContract) {
    try{
        const genres = await Database
        .from('genres') // 👈 gives an instance of select query builder
        .select('*')

        response.ok({
          data: genres
        })
      } catch (error) {
        response.badRequest(error.messages)
      }
  }

  public async create({}: HttpContextContract) {}

  public async store({response,request}: HttpContextContract) {

    try {

        const newGenres=await Database
            .table('genres')
            .insert({
              name:request.input("name"),
            })

        response.ok({
          response:'success',
          message: 'data genre berhasil di input'
        })
      } catch (error) {
          response.badRequest(error.messages)
      }
  }

  public async show({response,params}: HttpContextContract) {

    try {

        const genres = await Database
        .from('genres')
        .where('id',params.id)
        .first()

        const movies = await Database.rawQuery('select id,title,resume,DATE_FORMAT(release_date, "%Y-%m-%d") as release_date from movies where genre_id='+params.id)

        const res={
          id:genres.id,
          name:genres.name,
          movies:movies[0],

        }

        response.ok(res)

      } catch (error) {
        response.badRequest(error.messages)
      }
  }

  public async edit({}: HttpContextContract) {}

  public async update({response,request,params}: HttpContextContract) {

    try {

      const genres=await Database
        .from('genres')
        .where('id',params.id)
        .update({
          name:request.input("name"),
        })

      response.ok({
        response:'success',
        message: "data berhasil diubah",
        data:genres
      })
    } catch (error) {
      response.badRequest(error.messages)
    }

  }

  public async destroy({response,params}: HttpContextContract) {
    try {

      const genres = await Database
        .from('genres')
        .where('id',params.id)
        .delete()

      response.ok({
        response:'success',
        message: 'data berhasil dihapus'
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }
}
