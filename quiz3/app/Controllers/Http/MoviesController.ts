import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database';

export default class MoviesController {
  public async index({response}: HttpContextContract) {

    try {

      const venues = await Database.rawQuery('select id,title,resume,DATE_FORMAT(release_date, "%Y-%m-%d") as release_date,genre_id from movies')
      response.ok({
        response:'success',
        data: venues[0]
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async create({}: HttpContextContract) {}

  public async store({response,request}: HttpContextContract) {
    try {

      const newMovies=await Database
          .table('movies')
          .insert({
            title:request.input("title"),
            resume:request.input("resume"),
            release_date:request.input("release_date"),
            genre_id:request.input("genre_id"),
          })

      response.ok({
        response:'success',
        message: 'data movies berhasil di input',
        data:newMovies
      })
    } catch (error) {
        response.badRequest(error.messages)
    }
  }

  public async show({response,params}: HttpContextContract) {


    try {

      const venues = await
      Database.rawQuery('select m.id,m.title,m.resume,DATE_FORMAT(m.release_date, "%Y-%m-%d") as release_date,g.name as genre from movies m inner join genres g ON m.genre_id=g.id  where m.id='+params.id)

      response.ok(venues[0])
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async edit({}: HttpContextContract) {}

  public async update({response,request,params}: HttpContextContract) {

    try {

      const newMovies=await Database
          .from('movies')
          .update({
            title:request.input("title"),
            resume:request.input("resume"),
            release_date:request.input("release_date"),
            genre_id:request.input("genre_id"),
          })
          .where('id',params.id)

      response.ok({
        response:'success',
        message: 'data movies berhasil di update',
        data:newMovies
      })
    } catch (error) {
        response.badRequest(error.messages)
    }
  }

  public async destroy({response,params}: HttpContextContract) {

    try {

      const movies = await Database
        .from('movies')
        .where('id',params.id)
        .delete()

      response.ok({
        response:'success',
        message: 'data movies berhasil dihapus'
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }
}
