import Kelas from "./kelas"

class BootcampErwin{
    constructor(nama){
        this._name=nama
        this._kelas = []
    } 

    get name(){
        return this._name
    }

    set name(name){
        this._name=name
    }

    get kelas(){
        return this._kelas
    }

    createClass(name,level,instructor){
        let newKelas= new Kelas(
            name,
            level,
            instructor
        )
        this._kelas.push(newKelas)
    }
}

export default Bootcamp;