class Kelas{
    constructor(name,level,instructor){
        this._name = name
        this._level = level
        this._instructor = instructor
        this._students = []
    }

    get name(){
        return this._judul
    }

    set judul(judul){
        this._judul = this.judul
    }

    get level(){
        return this._level
    }

    set level(level){
        this._level = level
    }

    get instructor(){
        return this._instructor
    }

    set instructor(instructor){
        this._instructor = instructor
    }

}

export default Kelas;